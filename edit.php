<?php
require "header.php";
?>

<br/>
<br/>
<br/>
<textarea id="txtarea1"  rows=10 cols=60 f="difference()" onkeyup="difference()">
Hello
Good Bye
ABCD EFGH</textarea>
<input type="button" value="store in var" onclick="txtarea_to_var()"/>
<input type="button" value="difference" onclick="difference()"/>
<script type="text/javascript">
    var id='<?php echo time(); ?>';
    var selected_file="file.txt";
    /*
  lines[0]=1
    .
    .
    .
 x[0][name]='ssss'
 x[0][content]=lines;
    
    
    Types of difference operations:
    D: delete all the lines after the given index
    M: modify an existing line
    
     */  
    var files=new Array();
    var differences=new Array();
    function txtarea_to_var(){
    
        var i=0;
        var flag=false;
        for(i=0;i<files.length;i++){
        
            if(files[i]['name']==selected_file){
                flag=true;
                break;
            }

        }
    
    
    
        if(flag!=true){
            i=files.length;
        }
    
    
        files[i]=new Array();
        files[i]['name']=selected_file;
        var lines;
        var txt=$('#txtarea1').val();
        txt=txt.replace("\r\n",'\n');
        lines=txt.split('\n');
        files[i]['content']=lines;
    
        //console.log(files);
    }    
    
    function difference(){
    
        var txt=$('#txtarea1').val();
        var lines;
        txt=txt.replace("\r\n",'\n');
        lines=txt.split('\n');
        var new_text=lines;
        var jstr=""; //json string
    
        var i=0;
        var orig;
        var orig_file_index=0;
        
        for(i=0;i<files.length;i++){
            
            if(files[i]['name']==selected_file){
                
                orig=files[i]['content'];
                orig_fie_index=i;
                break;
            }
            
        }
        
        var dif={};    
        var isdif=false;
        if(new_text.length<orig.length){
            
                console.log("Lines deleted from :"+(new_text.length)); 
                isdif=true;
            dif={};    
            dif.type='D';
            dif.file=selected_file;
            dif.by=id;
            dif.diff='';
            dif.pos=new_text.length;
            differences.push(dif);       
        
        }


        for(i=0;i<new_text.length;i++){
        
            if(i>(orig.length-1)){//&& orig[i]==new_text[i]){
                console.log("new line difference at line :"+i);
                isdif=true;
                dif={};
                dif.type='M';
                dif.file=selected_file;
                dif.by=id;
                dif.diff=new_text[i];
                dif.pos=i;
                differences.push(dif);
            }
            else if(orig[i]==new_text[i]){
                //
            }
            else{
                isdif=true;
                dif={};
                dif.type='M';
                dif.file=selected_file;
                dif.by=id;
                dif.diff=new_text[i];
                dif.pos=i;
                differences.push(dif);
                console.log("diff at line :" + i);
            
            }
        }    
        
        
        
        files[orig_file_index]['content']= new_text;  
        
        
        if(isdif==true){
             jstr=JSON.stringify(differences);
            socket.send(jstr);
            differences=new Array();
        }
    
    }    
    
    


    var host = "ws://localhost:5001";
    var socket=null;
    try{
        socket = new WebSocket(host);
        /*
  console.log('WebSocket - status '+socket.readyState);
  socket.onopen    = function(msg){ console.log("Welcome - status "+this.readyState); };
  socket.onmessage = function(msg){ console.log("Received: "+msg.data); };
  socket.onclose   = function(msg){ console.log("Disconnected - status "+this.readyState); };
         */
    }
    catch(ex){ log(ex); }

    var data={};
    function send_data(evt){
        //console.log(evt+" "+getCaretPos('txtarea1'));
        //alert(evt.keyCode);
        data['cid']=id;
        data['selected_file']=selected_file;
        data['caret_pos']=getCaretPos('txtarea1');
        data['text']=evt.keyCode;
        //console.log(data);
        //	socket.send(JSON.stringify(data));

    }



    var setCaretTo = function(id, pos) {
        var obj = document.getElementById(id); 
        if(obj.createTextRange) { 
            var range = obj.createTextRange(); 
            range.move("character", pos); 
            range.select(); 
        } else if(obj.selectionStart) { 
            obj.focus(); 
            obj.setSelectionRange(pos, pos); 
        } 
    };
    
    var getCaretPos = function(id) {
        var el = document.getElementById(id);
        var rng, ii=-1;
        if(typeof el.selectionStart=="number") {
            ii=el.selectionStart;
        } else if (document.selection && el.createTextRange){
            rng=document.selection.createRange();
            rng.collapse(true);
            rng.moveStart("character", -el.value.length);
            ii=rng.text.length;
        }
        return ii;
    };
</script>
<?php
require "footer.php";
?>
