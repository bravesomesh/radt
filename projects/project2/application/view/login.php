<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
?>
<html>
    <head>
        <title>login</title>
    </head>
    <body>
<center><p id="status"></p></center>
<h1 style="text-align: center; ">
	<span style="font-family:trebuchet ms,helvetica,sans-serif;"><tt><strong>LOGIN</strong></tt></span></h1>

<p style="text-align: center; ">
	Username:<input id="username" name="username" type="text" /></p>
<p style="text-align: center; ">
	Password:<input id="password" name="password" type="text" /></p>
<p style="text-align: center; ">
	<input id="login" name="login" type="button" value="Login" /><input id="Register" name="Register" type="button" value="Register" /></p>
<div>
	&nbsp;</div>
<p>
	&nbsp;</p>

    </body>
</html>
