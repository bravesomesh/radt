<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
?>
<html>
    <head>
        <title>test_events</title>
    </head>
    <body>
        <p id="status">hello from test_events</p>
        
        <input type="button" id="onmousedown" value="test onmousedown" />
        <p>
            Test Onchange:
            <input type="text" id="onchange" value='test'/>
            
        </p>
         <input type="button" id="onmouseup" value="test onmouseup" />
        
        
        <p><input type="button" id="button_one" value="click to change above text!" /></p>
        </p>
         
        <p><input type="submit" id="onmouseover" value="test onmouseover" /></p>
         <p><input type="button" id="ondblclick" value="test ondblclick" /></p>
         
        <p><input type="button" id="onmousemove" value="test onmousemove" /></p>
        <p><input type="button" id="onmouseout" value="test onmouseout" /></p>
        
        <p><input type="text" id="onblur" value="test onblur" /></p>
        
        <p><input type="text" id="onfocus" value="test onfocus" /></p>
        
        <p><input type="text" id="onkeypress" value="test onkeypress" /></p>
        <p><input type="text" id="onkeyup" value="test onkeyup" /></p>
        <p><input type="text" id="onkeydown" value="test onkeydown" /></p>
        <select id="onselect">
            <option  value="select">ahgdf</option>
        </select>
    </body>
</html>