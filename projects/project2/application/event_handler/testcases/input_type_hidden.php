<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 *default_eventhandler
 *
 * @author silva
 */
class handler_input_type_hidden extends input_type_hidden{
    
    function setValue__onclick() {

        $rd = getComponent('hide');
        $rd->setValue('hello');
    }

    function getValue__onclick() {

        $rd = getComponent('hide');
        $t = $rd->getValue();

        $status = getComponent('status');
        $status->setHTML($t);
    }
    
}
