<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 *default_eventhandler
 *
 * @author silva
 */
class handler_input_checkbox extends input_checkbox{
    
     function setValue__onclick() {

        $rd = getComponent('chk');
        $rd->setValue('hello');
    }

    function getValue__onclick() {

        $rd = getComponent('chk');
        $t = $rd->getValue();

        $status = getComponent('status');
        $status->setHTML($t);
    }

    function disabled_true__onclick() {
        $x = getComponent('chk');
        $x->disabled(true);
    }

    function hidden_true__onclick() {
        $x = getComponent('chk');
        $x->hidden(true);
    }

    function hidden_false__onclick() {
        $x = getComponent('chk');
        $x->hidden(false);
    }

    function disabled_false__onclick() {
        $x = getComponent('chk');
        $x->disabled(false);
    }

    function checked_true__onclick() {
        $x = getComponent('chk');
        $x->checked(true);
    }

    function checked_false__onclick() {
        $x = getComponent('chk');
        $x->checked(false);
    }
}
