<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 *default_eventhandler
 *
 * @author silva
 */
class handler_login extends login{
    
	function Register__onclick(){

		radt_component::redirect("register");
	}

	function login__onclick(){
			
		$u=getComponent('username')->getValue();
		$p=getComponent('password')->getValue();

		$user=User::find_by_username($u);

		if($user==null || $user->password!=$p){
			
			getComponent('status')->setHTML("<font color=red>Login Failed</font>");
		}
		else{
			
			getComponent('status')->setHTML("<font color=green>Login Succeeded!</font>");			
	
		}
	}
    
}
