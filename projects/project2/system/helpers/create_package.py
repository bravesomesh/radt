import sys
import os

pack_name=""
base_dir="";
create=0;
print sys.argv

if(len(sys.argv)>1):
	pack_name=sys.argv[1];
	create=1;
	if(len(sys.argv)>2):
		base_dir=sys.argv[2];	
	
else:
	print "not enough args"
	exit()

slen=len(base_dir)

if(slen>0):
	if(base_dir[slen-1]!="/"):
		base_dir=base_dir+"/"

print base_dir


if(create==1):
	if(os.path.isfile("../../application/controller/"+base_dir+pack_name+".php") or os.path.exists("../../application/controller/"+base_dir+pack_name)):
		print "ERROR: Package or directory with the same name already exists"
	else:
		c_f=open("defaults/default_controller.php")
		c_c=c_f.read()
		c_c=c_c.replace('default_controller',pack_name)
		c_c=c_c.replace('base_dir',base_dir)
		c_f=open("../../application/controller/"+base_dir+pack_name+".php","w")
		c_f.write(c_c);

		c_f=open("defaults/default_model.php")
		c_c=c_f.read()
		c_c=c_c.replace('default_controller',pack_name)
		c_f=open("../../application/model/"+base_dir+pack_name+".php","w")
		c_f.write(c_c);

		c_f=open("defaults/default_view.php")
		c_c=c_f.read()
		c_c=c_c.replace('default_controller',pack_name)
		c_f=open("../../application/view/"+base_dir+pack_name+".php","w")
		c_f.write(c_c);


		c_f=open("defaults/default_eventhandler.php")
		c_c=c_f.read()
		c_c=c_c.replace('default_controller',pack_name)
		c_f=open("../../application/event_handler/"+base_dir+pack_name+".php","w")
		c_f.write(c_c);
		print c_c



