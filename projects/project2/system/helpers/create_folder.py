import sys
import os

base_dir="";
create=0;
print sys.argv


if(len(sys.argv)>1):
	base_dir=sys.argv[1];
	create=1;
else:
	print "not enough args"

if(create==1):
	if(os.path.isfile("../../application/controller/"+base_dir+".php") or os.path.exists("../../application/controller/"+base_dir)):
		print "ERROR: Package or directory with the same name already exists"
	else:
		os.mkdir("../../application/controller/"+base_dir)
		os.mkdir("../../application/model/"+base_dir)
		os.mkdir("../../application/view/"+base_dir)
		os.mkdir("../../application/event_handler/"+base_dir)
