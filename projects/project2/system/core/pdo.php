<?php

class RADT_PDO{

public $pdo=null;

	function __construct(){
	
		global $config;		
	   if(!class_exists('PDO',false))
		 fatal_error("PHP PDO package is required.");
		 
	   if(empty($config))
		 fatal_error("database definitions required.");

	   if(empty($config['charset']))
		$config['charset'] = 'UTF-8';
		 
		/* attempt to instantiate PDO object and database connection */
		try {    
		  $this->pdo = new PDO(
			"{$config['type']}:host={$config['host']};dbname={$config['name']};charset={$config['charset']}",
			$config['user'],
			$config['pass'],
			array(PDO::ATTR_PERSISTENT => !empty($config['persistent']) ? true : false)
			);
		  $this->pdo->exec("SET CHARACTER SET {$config['charset']}"); 
		} catch (PDOException $e) {
			fatal_error(sprintf("Can't connect to PDO database '{$config['type']}'. Error: %s",$e->getMessage()));
		}
		
		// make PDO handle errors with exceptions
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);    
		
	}
	
	

}