<?php


  class eventHandler{
  
	  public $raw_methods=array();
	  public $registered_methods=array();
	  public $js_code;
	  public $__events=array("onclick","onmousedown","onchange","onmouseup","onmouseover","ondblclick","onmousemove","onmouseout","onblur","onfocus","onkeypress","onkeyup","onkeydown","onselect");
	  private $method_inc=0;
	//---------------------------------------------------------
	function __construct($class_path){
	  
		$this->class_path=$class_path;
	 
	}
	//-------------------------------------------------------  
	  function get_methods($class_name){
	  
		$this->raw_methods=get_class_methods($class_name);
		
		foreach($this->raw_methods as $method){
		
		$this->register_method($method);
		
		}
		
		return $this->raw_methods;
	  
	  }
	//----------------------------------------------------------------------
	  function generate_url($method){
	  
		return base_path()."/".str_replace(array("application/controller/",".php"),"",$this->class_path)."/".$method;
	  
	  }
	//-----------------------------------------------------------------------
	  function register_method($method){
	  
		$fragments=explode("__",$method);
		$type=end($fragments);
		
		if(in_array($type,$this->__events)){
		
			$this->registered_methods[$this->method_inc]["type"]=$type;
			$this->registered_methods[$this->method_inc]["name"]=$method;
			$this->registered_methods[$this->method_inc]["id"]=str_replace("__".$type,"",$method);
			$this->method_inc++;
		
		}
		
	  }
	//------------------------------------------------------------------------
	  function generate_client_code(){
	  
		foreach($this->registered_methods as $method){
		
		$val=$method;
			if($val["type"]=='onclick'){
			
				$this->js_code.=$this->onclick_code($val["id"],$val["name"]);
			
			}
			else if($val["type"]=='onchange'){
			
				$this->js_code.=$this->onchange_code($val["id"],$val["name"]);
			
			}
                        else if($val["type"]=='onmousedown'){
                            
				$this->js_code.=$this->onmousedown_code($val["id"],$val["name"]);                            
                            
                        }
                        else if($val["type"]=='onmouseup'){
                            
				$this->js_code.=$this->onmouseup_code($val["id"],$val["name"]);      
                                
                            
                        } else if ($val["type"] == 'onmouseover') {

                  $this->js_code.=$this->onmouseover_code($val["id"], $val["name"]);
                       
                  
                        } else if ($val["type"] == 'ondblclick') {

                            $this->js_code.=$this->ondblclick_code($val["id"], $val["name"]);
           
                         
                            
                         } else if ($val["type"] == 'onmousemove') {

                            $this->js_code.=$this->onmousemove_code($val["id"], $val["name"]);
                         
                            
                         } else if ($val["type"] == 'onmouseout') {

                             $this->js_code.=$this->onmouseout_code($val["id"], $val["name"]);
            
                            
                                
                         } else if ($val["type"] == 'onblur') {

                             $this->js_code.=$this->onblur_code($val["id"], $val["name"]);
            
                             }
                           else if ($val["type"] == 'onfocus') {

                             $this->js_code.=$this->onfocus_code($val["id"], $val["name"]);
            
                             }
                             else if ($val["type"] == 'onkeypress') {

                             $this->js_code.=$this->onkeypress_code($val["id"], $val["name"]);
            
                             }
                             else if ($val["type"] == 'onkeyup') {

                             $this->js_code.=$this->onkeyup_code($val["id"], $val["name"]);
            
                             }
                             else if ($val["type"] == 'onkeydown') {

                             $this->js_code.=$this->onkeydown_code($val["id"], $val["name"]);
            
                             }
                             else if ($val["type"] == 'onselect') {

                             $this->js_code.=$this->onselect_code($val["id"], $val["name"]);
            
                             }
            
            }
    }

    //-----------------------------------------------------------------------
    function onclick_code($id, $name) {
	  
		$code="\n\n$('#".$id."').click(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
          
           //----------------------------------------------------    
          function onmousedown_code($id,$name){
	  
		$code="\n\n$('#".$id."').mousedown(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
          //----------------------------------------------------------------------
            function onblur_code($id,$name){
	  
		$code="\n\n$('#".$id."').blur(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
          
          //----------------------------------------------------------------------
            function onfocus_code($id,$name){
	  
		$code="\n\n$('#".$id."').focus(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
          
          
      //---------------------------------------------------------------------------    
          function onmouseover_code($id,$name){
	  
		$code="\n\n$('#".$id."').mouseover(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
          
            //----------------------------------------------------    
          function ondblclick_code($id,$name){
	  
		$code="\n\n$('#".$id."').dblclick(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
          
           //----------------------------------------------------    
          function onmousemove_code($id,$name){
	  
		$code="\n\n$('#".$id."').mousemove(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
          
	//-----------------------------------------------------------------------
	  function onmouseout_code($id,$name){
	  
		$code="\n\n$('#".$id."').mouseout(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }     
	//-----------------------------------------------------------------------
	  function onmouseup_code($id,$name){
	  
		$code="\n\n$('#".$id."').mouseup(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }             
	//-----------------------------------------------------------------------
	  function onchange_code($id,$name){
	  
		$code="\n\n$('#".$id."').change(function(){
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});		

		});\n\n";
		return $code;
		
	  }
           //----------------------------------------------------    
          function onkeypress_code($id,$name){
	  
		$code="\n\n$('#".$id."').keypress(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
          
          //----------------------------------------------------    
          function onkeyup_code($id,$name){
	  
		$code="\n\n$('#".$id."').keyup(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
            //----------------------------------------------------    
          function onkeydown_code($id,$name){
	  
		$code="\n\n$('#".$id."').keydown(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
             //----------------------------------------------------    
          function onselect_code($id,$name){
	  
		$code="\n\n$('#".$id."').select(function(){
		//alert('click event success');
			radt_reregister_components();
			
			$.post('".$this->generate_url($name)."',{_radt_components:_radf_component_array_string},function(data){
			eval(data);
			});
		
		});\n\n";
		return $code;
		
	  }
	//--------------------------------------------------------------------------
	  function get_code(){
	  
		return "\n<script type='text/javascript'>\n".file_get_contents('file.js').$this->js_code."\n</script>\n";
	  }
	
  }