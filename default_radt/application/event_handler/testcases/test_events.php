<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 *default_eventhandler
 *
 * @author silva
 */
class handler_test_events extends test_events{
    
    public function button_one__onclick(){
        $component=getComponent('status');
        $component->setHTML("<b>Text Change Successfull!</b>");
    }
    
    function onmousedown__onmousedown(){
                $component=getComponent('status');
        $component->setHTML("<b>onmousedown Successfull!</b>");
    }
    function onmouseup__onmouseup(){
                $component=getComponent('status');
        $component->setHTML("<b>onmouseup Successfull!</b>");
    }    
    function onchange__onchange(){
                $component=getComponent('status');
        $component->setHTML("<b>onchange Successfull!</b>");        
    }
    
    
     function onmouseover__onmouseover(){
                $component=getComponent('status');
        $component->setHTML("<b>onmouseover Successfull!</b>");  
     }
        
        
     function ondblclick__ondblclick(){
                $component=getComponent('status');
        $component->setHTML("<b>ondblclick Successfull!</b>");        
    }
  
    function onmousemove__onmousemove(){
                $component=getComponent('status');
        $component->setHTML("<b>onmousemove Successfull!</b>");        
    }
    
    
     function onmouseout__onmouseout(){
                $component=getComponent('status');
        $component->setHTML("<b>onmouseout Successfull!</b>");        
    }
    
    
      function onblur__onblur(){
                $component=getComponent('status');
        $component->setHTML("<b>onblur Successfull!</b>");    
    
        }

      function onfocus__onfocus(){
                $component=getComponent('status');
        $component->setHTML("<b>onfocus Successfull!</b>");        
    }
    
    function onkeypress__onkeypress(){
                $component=getComponent('status');
        $component->setHTML("<b>onkeypress Successfull!</b>");        
    }
        
      function onkeyup__onkeyup(){
                $component=getComponent('status');
        $component->setHTML("<b>onkeyup Successfull!</b>");        
    }    
    
    
    function onkeydown__onkeydown(){
                $component=getComponent('status');
        $component->setHTML("<b>onkeydown Successfull!</b>");        
    }    

    function onselect__onselect(){
                $component=getComponent('status');
        $component->setHTML("<b>onselect Successfull!</b>");        
    }    
}

