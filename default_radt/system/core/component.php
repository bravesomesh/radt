<?php

if (isset($_POST['_radt_components'])) {

    $compnts = json_decode($_POST['_radt_components']);
    global $radt_components;
    foreach ($compnts as $compnt) {


        $radt_components[$compnt->id] = $compnt;
    }
}

function getComponent($id) {

    global $radt_components;
    //var_dump($radt_components);
    foreach ($radt_components as $radt_component) {

        if ($radt_component->id == $id) {

            $comp = new radt_component($id);
            $vars = get_object_vars($radt_component);

            foreach ($vars as $key => $val) {
                if ($key == 'id') {
                    $key = '#' . $key;
                }
                $comp->$key = $val;
            }

            return $comp;
        }
    }
    return new radt_component($id);
}

class radt_component {

    public $js_code;

    function __construct($id) {

        global $EVENT_EVAL_CODE;
        $this->raw_id = $id;
        $this->id = '#' . $id;
        $this->js_code = &$EVENT_EVAL_CODE;
    }

    function setHTML($html) {


        $this->html = $html;
        $this->js_code.="\n$('" . $this->id . "').html('" . str_replace("'", "\\'", $html) . "');";
    }

    function appendHTML($html) {

        if (!isset($this->html)) {
            $this->html = "";
        }

        $this->html.=$this->html;
        $this->js_code.="\n$('" . $this->id . "').append('" . str_replace("'", "\\'", $html) . "');";
    }

    function getHTML() {

        if (!isset($this->html)) {
            $this->html = "";
        }
        return $this->html;
    }

    function setValue($value) {

        $this->value = $value;
        $this->js_code.="\n$('" . $this->id . "').val('" . str_replace("'", "\\'", $value) . "');";
    }

    function getValue() {
        return $this->value;
    }

    function checked($checked=null) {

        if ($checked === null) {
            if (!isset($this->checked)) {
                return false;
            }

            return $this->checked;
        }


        if (!isset($this->checked)) {
            $this->checked = false;
        }
        $this->checked = $checked;
        if($checked==false){
            $this->js_code.="\n$('" . $this->id . "').removeAttr('checked')";
            return;
            
        }
        
        $this->js_code.="\n$('" . $this->id . "').attr('checked','" . str_replace("'", "\\'", $checked) . "');";
    }

    function disabled($disabled=null) {

        if ($disabled === null) {
            if (!isset($this->disabled)) {
                return false;
            }

            return $this->disabled;
        }




        $this->disabled = $disabled;
        $this->js_code.="\n$('" . $this->id . "').attr('disabled','" . str_replace("'", "\\'", $disabled) . "');";
    }

    function hidden($hidden=null) {

        if ($hidden === null) {
            if (!isset($this->hidden)) {
                return false;
            }

            return $this->hidden;
        }




        $this->hidden = $hidden;

        if ($hidden == true) {
            $this->js_code.="\n$('" . $this->id . "').hide();";
        } else {
            $this->js_code.="\n$('" . $this->id . "').show();";
        }
    }

    function hide() {
        $this->js_code.="\n$('" . $this->id . "').hide();";
    }

    function show() {
        $this->js_code.="\n$('" . $this->id . "').show();";
    }

    function add_custom_js($js) {
        $this->js_code.="\n $js";
    }

    /**
     * Add an option to the select element.
     *
     * @category   Select
     * @param  string  $option option to be added
     * @param  string  $value  value of the option,set it to null if not used[optional]
     * @param  boolean  $selected   option is selected or not[optional]
     * @return void
     */
    function add_option($option, $value=null, $selected=false) {

        $option = str_replace("'", "\\'", $option);

        if ($value === null) {
            $this->js_code.="\n$('" . $this->id . "').append('<option>" . $option . "</option>');";
            
        } else {
            $value = str_replace("'", "\\'", $value);
            $this->js_code.="\n$('" . $this->id . "').append('<option value=\"".$value."\">" . $option . "</option>');";
        }
    }
    
    
    /**
     * get selected option(s).
     * returns an object if only one is selected.
     * returns an array if multiple options are selected.
     * return false if none are selected.
     * @category   select
     * @return  object|array|boolean
     *  
     */
    function get_selected_options(){
        
        if(count($this->selections)<=0){
            return false;
        }
        
        $array=array();
        
        if(count($this->selections)==1){
            
            return $this->selections[0];
        }
        
        return $this->selections;
            
            
        
    }
    
    
    /**
     * Clears all the options in the select element.
     *
     * @category   Select
     * @return void
     */    
    function clear_options(){
        
        $this->js_code.="\n$('" . $this->id . "').html('')";
    }
    
    
    /**
     * Set the attribute of an element
     * 
     * @param   string $attribute attribute key
     * @param   string $value attribute value
     * @category Allelements
     * @return void
     */
     function setAttribute($attribute,$value){
         
         $attribute=str_replace("'", "\\'", $attribute) ;
         $value=str_replace("'", "\\'", $value) ;
         
        $this->js_code.="\n$('" . $this->id . "').attr('$attribute','" . $value . "');";
         
     }
    
     
     
     public static function redirect($path){
         
	        global $EVENT_EVAL_CODE;
         $EVENT_EVAL_CODE.="\nwindow.location='".  base_path($path)."'; ";
         
     }
     
}
