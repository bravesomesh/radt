<?php

function get_projects($base="")
{
	$dirs=scandir($base."projects");
$projects=array();
	//var_dump($dirs);
	foreach($dirs as $dir){

	if($dir=="." || $dir==".." || $dir==".svn")
		continue;

		if(is_dir($base."projects/".$dir)){

			//echo $dir."<br/>";
			$projects[]=$dir;
		}
	}
return $projects;
}

//---------------------------------------------------------------------
//@project: name of the project
//@base_dir: current directory to be refreshed
//@prefix: change the base directory ,example prefix="../"
function get_packages($project,$basedir="",$prefix=""){

	$dir=scandir($prefix."projects/".$project."/application/model/".$basedir);
	
	$packages=array();
	
	foreach($dir as $file)
	{
		if (is_file($prefix."projects/".$project."/application/model/".$basedir.$file) && is_file($prefix."projects/".$project."/application/view/".$basedir.$file) && is_file($prefix."projects/".$project."/application/controller/".$basedir.$file) && is_file
		($prefix."projects/".$project."/application/event_handler/".$basedir.$file))
		
		{

		//echo $file;
			$packages[]=str_replace (".php","",$file);
		}
	}	
	return $packages;
}


//------------------------------------------------------------------------------
function get_package_folders($project,$basedir=""){

	$dir=scandir("projects/".$project."/application/model/".$basedir);
	
	$packages=array();
	
	foreach($dir as $file)
	{
		if (is_dir("projects/".$project."/application/model/".$basedir."/".$file) && is_dir("projects/".$project."/application/view/".$basedir."/".$file) && is_dir("projects/".$project."/application/controller/".$basedir."/".$file) && is_dir("projects/".$project."/application/event_handler/".$basedir."/".$file)){

		//echo $file;
		if($file!="." && $file!="..")
			$packages[]=$file;
		}
	}
	
	return $packages;
}


//-----------------------------------------------------------------------------
function recurse_copy($src,$dst) { 
     $dir = opendir($src); 
     @mkdir($dst,0777); 
     @chmod($dst,0777);
     while(false !== ( $file = readdir($dir)) ) { 
         if (( $file != '.' ) && ( $file != '..' )) { 
             if ( is_dir($src . '/' . $file) ) { 
                 recurse_copy($src . '/' . $file,$dst . '/' . $file); 
             } 
             else { 
                 copy($src . '/' . $file,$dst . '/' . $file); 
                 chmod($dst . '/' . $file,0777);
             } 
         } 
     } 
     closedir($dir); 
 }

// ------------ lixlpixel recursive PHP functions -------------
// recursive_remove_directory( directory to delete, empty )
// expects path to directory and optional TRUE / FALSE to empty
// of course PHP has to have the rights to delete the directory
// you specify and all files and folders inside the directory
// ------------------------------------------------------------

// to use this function to totally remove a directory, write:
// recursive_remove_directory('path/to/directory/to/delete');

// to use this function to empty a directory, write:
// recursive_remove_directory('path/to/full_directory',TRUE);

function recursive_remove_directory($directory, $empty=FALSE)
{
	// if the path has a slash at the end we remove it here
	if(substr($directory,-1) == '/')
	{
		$directory = substr($directory,0,-1);
	}

	// if the path is not valid or is not a directory ...
	if(!file_exists($directory) || !is_dir($directory))
	{
		// ... we return false and exit the function
		return FALSE;

	// ... if the path is not readable
	}elseif(!is_readable($directory))
	{
		// ... we return false and exit the function
		return FALSE;

	// ... else if the path is readable
	}else{

		// we open the directory
		$handle = opendir($directory);

		// and scan through the items inside
		while (FALSE !== ($item = readdir($handle)))
		{
			// if the filepointer is not the current directory
			// or the parent directory
			if($item != '.' && $item != '..')
			{
				// we build the new path to delete
				$path = $directory.'/'.$item;

				// if the new path is a directory
				if(is_dir($path)) 
				{
					// we call this function with the new path
					recursive_remove_directory($path);

				// if the new path is a file
				}else{
					// we remove the file
					unlink($path);
				}
			}
		}
		// close the directory
		closedir($handle);

		// if the option to empty is not set to true
		if($empty == FALSE)
		{
			// try to delete the now empty directory
			if(!rmdir($directory))
			{
				// return false if not possible
				return FALSE;
			}
		}
		// return success
		return TRUE;
	}
}
// ------------------------------------------------------------

?>