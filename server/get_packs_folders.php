<?php
$current_dir=$_GET['current_dir'];
$project_dir=$_GET['project_dir'];
$files=scandir("../projects/".$project_dir."/application/controller/".$current_dir);

$list=array();

$i=0;

foreach ($files as $file)
{
    if ($file=="." || $file==".." ||  $file==".svn"){
        continue;
    }
    
    if (is_file("../projects/".$project_dir."/application/controller/".$current_dir."/".$file) &&
            is_file("../projects/".$project_dir."/application/model/".$current_dir."/".$file)  &&
            is_file("../projects/".$project_dir."/application/view/".$current_dir."/".$file) &&
            is_file("../projects/".$project_dir."/application/event_handler/".$current_dir."/".$file) ){
        $list[$i]['name']=str_replace(".php","",$file);
        $list[$i]['path']=null;
        $list[$i]['type']='file';
	$i++;	
	continue;
    } 
    else if (is_dir("../projects/".$project_dir."/application/controller/".$current_dir."/".$file) &&
            is_dir("../projects/".$project_dir."/application/model/".$current_dir."/".$file)  &&
            is_dir("../projects/".$project_dir."/application/view/".$current_dir."/".$file) &&
            is_dir("../projects/".$project_dir."/application/event_handler/".$current_dir."/".$file) ){
        $list[$i]['name']=$file;
        $list[$i]['path']=$current_dir."/".$file;
        
        if($list[$i]['path'][0]=='/'){
           $list[$i]['path']= substr($list[$i]['path'],1);
        }
        
        $list[$i]['type']='dir';
 	$i++;
	continue;
    } 
    
       // echo $file."<br/>";
    
    
    
  
}
    


echo json_encode($list);
