<?php
$filename=$_REQUEST['filename'];
$type=$_REQUEST['type'];
//var_dump($_REQUEST);
?>
<html>
    
    <head>
        <script src="../js/jquery.1.6.js"></script>
        <script src="../ace/src/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="../ace/src/theme-textmate.js" type="text/javascript" charset="utf-8"></script>
<script src="../ace/src/mode-php.js" type="text/javascript" charset="utf-8"></script>
<script>
    var my_editor;

var ace_session;
window.onload = function() {
    
     my_editor = ace.edit("prearea1");
    my_editor.setTheme("ace/theme/textmate");
    
    var phpmode = require("ace/mode/php").Mode;
    my_editor.getSession().setMode(new phpmode());
        ace_session=my_editor.getSession();
        //ace_session.setValue($('#contents').val());
}
    </script>
        <title>
            Options
        </title>
    </head>
    <body>
        
        <?php
        
        $contents="";
        $show_save=false;
    
        if(strpos($filename,'.php')!==false
           ||strpos($filename,'.txt')!==false    
           ||strpos($filename,'.html')!==false 
           ||strpos($filename,'.htaccess')!==false
           ||strpos($filename,'.py')!==false
           ||strpos($filename,'.js')!==false 
           ||strpos($filename,'.css')!==false 
           ||strpos($filename,'.log')!==false 
           ||strpos($filename,'.ini')!==false){
            
            $show_save=true;
        if(is_file($filename)){
                $contents=file_get_contents($filename);
        }            
        }

    ?>
        <input type="text" id="filename" name="filename" value="<?php echo $filename; ?>" style="width:50%"/> 
        <input type="hidden" id="hidfilename" name="hidfilename" value="<?php echo $filename; ?>" /> 
        <input type="button" value="Rename" onclick="rename_file()"/>
        <input type="button" value="Delete" onclick="raw_delete_file()"/>
        <?php
        
        $display="none";
        if($show_save===true){
        $display="block";    
        echo '<input type="button" value="Save" onclick="raw_save_file()" />';
        }?><p>
            
            <pre style="height:90%;width:98%;display:<?php echo $display;?>;" id="prearea1"><?php echo htmlentities($contents); ?></pre>
            <textarea id="contents" style="display:none"></textarea>
            
        </p>
        <script type="text/javascript">
            function rename_file(){
                
                var nname=$('#filename').val();
                var orig=$('#hidfilename').val();
                
                $.get("rename_file.php",{orig:orig,nname:nname},function(data){
                    
                    alert(data);
                    if(data=='Rename Successfull.'){
                        $('#hidfilename').val($('#filename').val());
                    }
                    
                });
                
            }
            
            
            function raw_delete_file(){
                 var nname=$('#filename').val();
                $.get("raw_delete_file.php",{nname:nname,type:'<?php echo $type; ?>'},function(data){
                    
                    alert(data);
                    if(data=='Delete Successfull.'){
                        document.write('Deleted. Please close this document and refresh the File/Folder List.');
                    }
                    
                });
                
            }                
            
            
            function raw_save_file(){
                var nname=$('#filename').val();
                var contents=ace_session.getValue();
                $.post("save_file_contents.php",{file:nname,contents:contents},function(data){
                    
                    alert(data);

                    
                });                
            }
            
            </script>
        
    </body>
</html>