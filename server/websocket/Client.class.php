<?php




  $_start = 0;
  $_subdata = '';
$profile = true;
  function profile_start() {
    global $profile;
    if ( $profile ) {
      global $_start, $_subdata;
      $_start = microtime(true);
      $_subdata = '';
    }
  }

  function profile_end( $data ) {
    global $profile, $_subdata;
    if ( $profile ) {
      global $_start;
      // open file
      $fd = fopen("profile.csv", "a");
      // write string
      fwrite($fd, ( microtime(true) - $_start ) . "," . $data . "\n");
      fwrite($fd, $_subdata );
      // close file
      fclose($fd);
    }
  }

  function profile_info( $q ) {
    global $profile, $_subdata, $_start;
    if ( $profile ) {
      $_subdata .= (microtime(true) - $_start) . ",\"- " . $q . "\"\n";
    }
  }


class Client {
	public $id;
        public $clientId; //User Primary Key.
	public $socket;
	public $handshake;
	public $pid;
        public $exists=true;
	public $opened_files=array();
        public $index=0;
        public $db;
        
        
        public $uofq; //update open file query
        public $sld; //select latest diffs
	
	function Client($id, $socket) {
		
            global $db;
                $this->id = $id;
		$this->socket = $socket;
		$this->handshake = false;
		$this->pid = null;
                $this->db=$db;
     
	}
	
	public function getId() {
		return $this->id;
	}
        
        public function prepareQueries(){
            echo "\nQueries prepared.\n";
           $this->uofq=$this->db->prepare("UPDATE `open_files` SET `modified_time`=?  WHERE filename=? AND opened_by=?");
           $query="SELECT file_sync.chars as chars, 
                    file_sync.type as type, 
                    file_sync.file_path as file_path,
                    file_sync.pos as pos,
                    file_sync.entry_time as entry_time
                    FROM file_sync, open_files
                    WHERE open_files.filename = file_sync.file_path
                    AND file_sync.entry_time > open_files.modified_time
                    AND file_sync.edited_by !=".$this->clientId."
                    AND open_files.opened_by =".$this->clientId."
                    ORDER BY entry_time ASC"; 
           $this->sld=$this->db->prepare($query);
        }
	
	public function getSocket() {
		return $this->socket;
	}
	
	public function getHandshake() {
		return $this->handshake;
	}
	
	public function getPid() {
		return $this->pid;
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function setSocket($socket) {
		$this->socket = $socket;
	}
	
	public function setHandshake($handshake) {
		$this->handshake = $handshake;
	}
	
	public function setPid($pid) {
		$this->pid = $pid;
	}
        
        public function register_file($filename){
            
            clearstatcache();
            $this->opened_files[$this->index]['name']=$filename;
            $time=filemtime("../".$filename);
            $this->opened_files[$this->index]['modified_time']=$time;
            $this->index++;
            
            $query="INSERT INTO open_files(opened_by,filename,modified_time) VALUES(".$this->clientId.",'$filename',$time)";
            //var_dump($query);
            //$this->db->query($query);
            
        }
        
        
        public function get_updates(){
            
            $str=array();

            /*
            
            $this->get_opened_files();
            
            
            
            foreach($this->opened_files as $file){
                
                $str[]=" (file_path='".$file['name']."' AND entry_time>".$file['modified_time'].") ";
                
            }
            
           /// var_dump($this);
           $diff=null;
           $differences=array();
            $SQLstring=implode(" OR ", $str);
            
            
            if($SQLstring==""){ //no files opened by user
                return $differences;
            }
            
            $query="SELECT * FROM file_sync 
                
                    WHERE ($SQLstring) AND edited_by!=".$this->clientId." ORDER BY entry_time ASC";
           */
            
            $differences=array();
           /*$query= "SELECT file_sync.chars as chars, 
                    file_sync.type as type, 
                    file_sync.file_path as file_path,
                    file_sync.pos as pos,
                    file_sync.entry_time as entry_time
                    FROM file_sync, open_files
                    WHERE open_files.filename = file_sync.file_path
                    AND file_sync.entry_time > open_files.modified_time
                    AND file_sync.edited_by !=".$this->clientId."
                    AND open_files.opened_by =".$this->clientId."
                    ORDER BY entry_time ASC"; */
           $res=$this->sld->execute();
           //$res=$this->db->query($query);

           var_dump($res.$this->clientId);
		
           

           
           while($result = $this->sld->fetch()){
           //foreach($res as $result){
               echo "\nU occured\n";
               $diff=null;
               $diff->type=$result['type'];
               $diff->file=$result['file_path'];
               $diff->diff=$result['chars'];
               $diff->pos=$result['pos'];
               
               $differences[]=$diff;
               
             // $query="UPDATE `open_files` 
              //    SET `modified_time` =".$result['entry_time']."  WHERE filename='".$result['file_path']."' AND opened_by=".$this->clientId;            
               //$this->db->query($query);
               $this->uofq->execute(array($result['entry_time'],$result['file_path'],$this->clientId));
           }

           return $differences;
        }
        
        
        public function get_opened_files(){
         
            $query="SELECT * FROM open_files WHERE opened_by=".$this->clientId;
            
            $result=$this->db->query($query);
            
            //var_dump($result);
            $this->index=0;
            $this->opened_files=array();
            foreach($result as $file){
                
                                     //clearstatcache();
            $this->opened_files[$this->index]['name']=$file['filename'];
            $this->opened_files[$this->index]['modified_time']=$file['modified_time'];
            $this->index++;
                
                
            }
            
            
        
        }
}
?>
