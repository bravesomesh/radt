#!/usr/bin/php -q
<?php
/**
 * A daemon of PHP Push WebSocket
 * Avinash D'Silva
 *
 */
error_reporting(E_ALL);
require_once 'Server.class.php';
require_once 'Client.class.php';
require '../../connect.php';
set_time_limit(0);

//192.168.1.3
$address = '127.0.0.1';
$port = 5001;
$verboseMode = true;

$server = new Server($address, $port, $verboseMode);
$server->run();
?>
