<?php
$dri=$_GET['dri'];
$project=$_GET['project_name'];
$cdir=$_GET['current_dir'];

 function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
       } 
     } 
     reset($objects); 
     rmdir($dir); 
   } 
 } 
 function recursive_remove_directory($directory, $empty=FALSE)
{
	// if the path has a slash at the end we remove it here
	if(substr($directory,-1) == '/')
	{
		$directory = substr($directory,0,-1);
	}

	// if the path is not valid or is not a directory ...
	if(!file_exists($directory) || !is_dir($directory))
	{
		// ... we return false and exit the function
		return 'A';

	// ... if the path is not readable
	}elseif(!is_readable($directory))
	{
		// ... we return false and exit the function
		return 'B';

	// ... else if the path is readable
	}else{

		// we open the directory
		$handle = opendir($directory);

		// and scan through the items inside
		while (FALSE !== ($item = readdir($handle)))
		{
			// if the filepointer is not the current directory
			// or the parent directory
			if($item != '.' && $item != '..')
			{
				// we build the new path to delete
				$path = $directory.'/'.$item;

				// if the new path is a directory
				if(is_dir($path)) 
				{
					// we call this function with the new path
					recursive_remove_directory($path);

				// if the new path is a file
				}else{
					// we remove the file
					unlink($path);
				}
			}
		}
		// close the directory
		closedir($handle);

		// if the option to empty is not set to true
		if($empty == FALSE)
		{
			// try to delete the now empty directory
			if(!rmdir($directory))
			{
				// return false if not possible
				return 'C';
			}
		}
		// return success
		return TRUE;
	}
}
echo "Folder Deleted Successfully!";

//system("python delete_folder.py $project $dri",$retval);

if($cdir==""){
    
}else{
    $cdir.="/";
}
echo "../projects/$project/$dri";
recursive_remove_directory("../projects/$project/application/controller/$dri");
recursive_remove_directory("../projects/$project/application/model/$dri");
recursive_remove_directory("../projects/$project/application/view/$dri");
recursive_remove_directory("../projects/$project/application/event_handler/$dri");

//header("Location: ../index.php");
