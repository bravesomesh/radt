import shutil
import sys
import os

prj_name=""
dir_name=""
delete=0
print sys.argv

if(len(sys.argv)>1):
	prj_name=sys.argv[1]
	dir_name=sys.argv[2];
	delete=1
else:
	print "not enough args"
	exit()

shutil.rmtree("../projects/"+prj_name+"/application/controller/"+dir_name)
shutil.rmtree("../projects/"+prj_name+"/application/model/"+dir_name)
shutil.rmtree("../projects/"+prj_name+"/application/view/"+dir_name)
shutil.rmtree("../projects/"+prj_name+"/application/event_handler/"+dir_name)
