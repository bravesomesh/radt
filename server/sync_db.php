<?php

class Utils
{
	

	private static $plural = array(
        '/(quiz)$/i'               => "$1zes",
        '/^(ox)$/i'                => "$1en",
        '/([m|l])ouse$/i'          => "$1ice",
        '/(matr|vert|ind)ix|ex$/i' => "$1ices",
        '/(x|ch|ss|sh)$/i'         => "$1es",
        '/([^aeiouy]|qu)y$/i'      => "$1ies",
        '/(hive)$/i'               => "$1s",
        '/(?:([^f])fe|([lr])f)$/i' => "$1$2ves",
        '/(shea|lea|loa|thie)f$/i' => "$1ves",
        '/sis$/i'                  => "ses",
        '/([ti])um$/i'             => "$1a",
        '/(tomat|potat|ech|her|vet)o$/i'=> "$1oes",
        '/(bu)s$/i'                => "$1ses",
        '/(alias)$/i'              => "$1es",
        '/(octop)us$/i'            => "$1i",
        '/(ax|test)is$/i'          => "$1es",
        '/(us)$/i'                 => "$1es",
        '/s$/i'                    => "s",
        '/$/'                      => "s"
    );

    private static $singular = array(
        '/(quiz)zes$/i'             => "$1",
        '/(matr)ices$/i'            => "$1ix",
        '/(vert|ind)ices$/i'        => "$1ex",
        '/^(ox)en$/i'               => "$1",
        '/(alias)es$/i'             => "$1",
        '/(octop|vir)i$/i'          => "$1us",
        '/(cris|ax|test)es$/i'      => "$1is",
        '/(shoe)s$/i'               => "$1",
        '/(o)es$/i'                 => "$1",
        '/(bus)es$/i'               => "$1",
        '/([m|l])ice$/i'            => "$1ouse",
        '/(x|ch|ss|sh)es$/i'        => "$1",
        '/(m)ovies$/i'              => "$1ovie",
        '/(s)eries$/i'              => "$1eries",
        '/([^aeiouy]|qu)ies$/i'     => "$1y",
        '/([lr])ves$/i'             => "$1f",
        '/(tive)s$/i'               => "$1",
        '/(hive)s$/i'               => "$1",
        '/(li|wi|kni)ves$/i'        => "$1fe",
        '/(shea|loa|lea|thie)ves$/i'=> "$1f",
        '/(^analy)ses$/i'           => "$1sis",
        '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i'  => "$1$2sis",
        '/([ti])a$/i'               => "$1um",
        '/(n)ews$/i'                => "$1ews",
        '/(h|bl)ouses$/i'           => "$1ouse",
        '/(corpse)s$/i'             => "$1",
        '/(us)es$/i'                => "$1",
        '/(us|ss)$/i'               => "$1",
        '/s$/i'                     => ""
    );

    private static $irregular = array(
        'move'   => 'moves',
        'foot'   => 'feet',
        'goose'  => 'geese',
        'sex'    => 'sexes',
        'child'  => 'children',
        'man'    => 'men',
        'tooth'  => 'teeth',
        'person' => 'people'
    );

    private static $uncountable = array(
        'sheep',
        'fish',
        'deer',
        'series',
        'species',
        'money',
        'rice',
        'information',
        'equipment'
    );

    public static function pluralize( $string )
    {
        // save some time in the case that singular and plural are the same
        if ( in_array( strtolower( $string ), self::$uncountable ) )
            return $string;

        // check for irregular singular forms
        foreach ( self::$irregular as $pattern => $result )
        {
            $pattern = '/' . $pattern . '$/i';

            if ( preg_match( $pattern, $string ) )
                return preg_replace( $pattern, $result, $string);
        }

        // check for matches using regular expressions
        foreach ( self::$plural as $pattern => $result )
        {
            if ( preg_match( $pattern, $string ) )
                return preg_replace( $pattern, $result, $string );
        }

        return $string;
    }

    public static function singularize( $string )
    {
        // save some time in the case that singular and plural are the same
        if ( in_array( strtolower( $string ), self::$uncountable ) )
            return $string;

        // check for irregular plural forms
        foreach ( self::$irregular as $result => $pattern )
        {
            $pattern = '/' . $pattern . '$/i';

            if ( preg_match( $pattern, $string ) )
                return preg_replace( $pattern, $result, $string);
        }

        // check for matches using regular expressions
        foreach ( self::$singular as $pattern => $result )
        {
            if ( preg_match( $pattern, $string ) )
                return preg_replace( $pattern, $result, $string );
        }

        return $string;
    }

    public static function pluralize_if($count, $string)
    {
        if ($count == 1)
            return $string;
        else
            return self::pluralize($string);
    }

	public static function squeeze($char, $string)
	{
		return preg_replace("/$char+/",$char,$string);
	}
}

$project=$_GET['project_name'];


$files=scandir("../projects/$project/application/model/sync");

$m_files=array();
$m_classes=array();
foreach($files as $file){
    
    if($file=="." || $file==".."){
        
        
    }
    else{
        $m_files[]="../projects/$project/application/model/sync/".$file;
        $m_classes[]=str_replace('.php', '', $file);
    }
    
}

//var_dump($m_files);
$i=0;

$sqls=array();
$dsqls=array();
foreach($m_files as $m_file){
    
    
    require_once $m_file;
    $cls=$m_classes[$i];
    $obj =new $cls();
    $tbl_name=strtolower(Utils::pluralize($cls));
    $dsqls[$i]="DROP TABLE IF EXISTS `".$tbl_name."`\n";
    
    $sqls[$i]="";
    $sqls[$i].="CREATE TABLE IF NOT EXISTS `$tbl_name` (\n";
    
      $obj_vars=get_object_vars($obj);
      $k=0;
      foreach($obj_vars as $obj_key=>$obj_var){
          if($k==0)
              $sqls[$i].="`$obj_key` ";
          else
                $sqls[$i].=",\n`$obj_key` ";
          
          foreach($obj_var as $var){
              $sqls[$i].=" $var ";
          }
          $k++;
          //$sqls[$i].=" ,\n";
      }
    $sqls[$i].=");";
    
    $i++;
}

//var_dump($sqls);
//var_dump($dsqls);

require "../projects/$project/system/core/config.php";
$pdo=null;
$config['charset']='UTF-8';
try {    
		  $pdo = new PDO(
			"{$config['type']}:host={$config['host']};dbname={$config['name']};charset={$config['charset']}",
			$config['user'],
			$config['pass'],
			array(PDO::ATTR_PERSISTENT => !empty($config['persistent']) ? true : false)
			);
		  $pdo->exec("SET CHARACTER SET {$config['charset']}"); 
		} catch (PDOException $e) {
			fatal_error(sprintf("Can't connect to PDO database '{$config['type']}'. Error: %s",$e->getMessage()));
		}

$db=$pdo;                
foreach($dsqls as $dsql){
    $db->query($dsql);
}                
      

foreach($sqls as $sql){
    $db->query($sql);
} 

$query="SHOW TABLES";


$res=$db->query($query);

$res=$res->fetchAll();

foreach($res as $r=>$s){
//echo $s[0];
$str='<?php
class '.ucfirst(Utils::singularize($s[0])).' extends ActiveRecord\Model
{
}';

file_put_contents("../projects/$project/application/model/".  ucfirst(Utils::singularize($s[0])).".php",$str);

}




echo "Tables and Classes in sync.";
