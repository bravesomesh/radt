﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.dialog.add( 'radio', function( editor )
{
    return {
        title : editor.lang.checkboxAndRadio.radioTitle,
        minWidth : 350,
        minHeight : 140,
        onShow : function()
        {
            delete this.radioButton;

            var element = this.getParentEditor().getSelection().getSelectedElement();
            if ( element && element.getName() == 'input' && element.getAttribute( 'type' ) == 'radio' )
            {
                this.radioButton = element;
                this.setupContent( element );
            }
        },
        onOk : function()
        {
            var editor,
            element = this.radioButton,
            isInsertMode = !element;

            if ( isInsertMode )
            {
                editor = this.getParentEditor();
                element = editor.document.createElement( 'input' );
                element.setAttribute( 'type', 'radio' );
            }

            if ( isInsertMode )
                editor.insertElement( element );
            this.commitContent( {
                element : element
            } );
        },
        contents : [
        {
            id : 'info',
            label : editor.lang.checkboxAndRadio.radioTitle,
            title : editor.lang.checkboxAndRadio.radioTitle,
            elements : [
            {
                id : 'name',
                type : 'text',
                label : editor.lang.common.name,
                'default' : '',
                accessKey : 'N',
                setup : function( element )
                {
                    this.setValue(
                        element.data( 'cke-saved-name' ) ||
                        element.getAttribute( 'name' ) ||
                        '' );
                },
                commit : function( data )
                {
                    var element = data.element;

                    if ( this.getValue() )
                        element.data( 'cke-saved-name', this.getValue() );
                    else
                    {
                        element.data( 'cke-saved-name', false );
                        element.removeAttribute( 'name' );
                    }
                }
            },
            {
                id : 'value',
                type : 'text',
                label : editor.lang.checkboxAndRadio.value,
                'default' : '',
                accessKey : 'V',
                setup : function( element )
                {
                    this.setValue( element.getAttribute( 'value' ) || '' );
                },
                commit : function( data )
                {
                    var element = data.element;

                    if ( this.getValue() )
                        element.setAttribute( 'value', this.getValue() );
                    else
                        element.removeAttribute( 'value' );
                }
            },
            
            
            
            
            
            
            
            
            {
                id : 'id',
                type : 'text',
                label :'ID(unique id of the element)',
                accessKey : 'I',
                'default' : '',
               
                 setup : function( element )
                {
                    this.setValue( element.getAttribute( 'id' ) || '' );
                },
                commit : function( data )
                {
                    var element = data.element;

                    // IE failed to update 'name' property on input elements, protect it now.
value = this.getValue();

                    if ( value && !( CKEDITOR.env.ie && value == 'on' ) )
                        element.setAttribute( 'id', value );
                }
            },
	
            
            
            
            
            
            
            
            
            
            
            
            {
                id : 'radio_select',
                type : 'select',
                label : 'select_radio_action',
                'default' : 'onmouseup',
						
                items :
                [
                [ 'onclick', 'onclick' ],
                [ 'onmousedown', 'onmousedown' ],
                [ 'onmouseup','onmouseup' ],                
                [ 'onchange', 'onchange' ],
                [ 'onblur','onblur' ],
                [ 'onfocus','onfocus' ],
                [ 'onmouseover','onmouseover' ],
                [ 'ondbclick','ondbclick' ]
                ]
						
						
            },
            {
                id : 'set_action',
                type : 'button',
                label : 'set action',
                onClick:function(){
                                                    
                    var id_val=CKEDITOR.dialog.getCurrent().getContentElement('info', 'id').getValue();
                    var action_val=CKEDITOR.dialog.getCurrent().getContentElement('info', 'radio_select').getValue();
                    //alert(id_val+action_val);
                                                
                                                
                    get_event_handler(id_val,action_val);
                                               
                    CKEDITOR.dialog.getCurrent().hide();
                                                 
                                                 
                }
            },
                                      
            {
                id : 'checked',
                type : 'checkbox',
                label : editor.lang.checkboxAndRadio.selected,
                'default' : '',
                accessKey : 'S',
                value : "checked",
                setup : function( element )
                {
                    this.setValue( element.getAttribute( 'checked' ) );
                },
                commit : function( data )
                {
                    var element = data.element;

                    if ( !( CKEDITOR.env.ie || CKEDITOR.env.opera ) )
                    {
                        if ( this.getValue() )
                            element.setAttribute( 'checked', 'checked' );
                        else
                            element.removeAttribute( 'checked' );
                    }
                    else
                    {
                        var isElementChecked = element.getAttribute( 'checked' );
                        var isChecked = !!this.getValue();

                        if ( isElementChecked != isChecked )
                        {
                            var replace = CKEDITOR.dom.element.createFromHtml( '<input type="radio"'
                                + ( isChecked ? ' checked="checked"' : '' )
                                + '></input>', editor.document );
                            element.copyAttributes( replace, {
                                type : 1, 
                                checked : 1
                            } );
                            replace.replace( element );
                            editor.getSelection().selectElement( replace );
                            data.element = replace;
                        }
                    }
                }
            }
            ]
        }
        ]
    };
});
