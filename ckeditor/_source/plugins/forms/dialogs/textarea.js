﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.dialog.add( 'textarea', function( editor )
{
    
   
	return {
		title : editor.lang.textarea.title,
		minWidth : 350,
		minHeight : 220,
		onShow : function()
		{
			delete this.textarea;

			var element = this.getParentEditor().getSelection().getSelectedElement();
			if ( element && element.getName() == "textarea" )
			{
				this.textarea = element;
				this.setupContent( element );
			}
		},
		onOk : function()
		{
			var editor,
				element = this.textarea,
				isInsertMode = !element;

			if ( isInsertMode )
			{
				editor = this.getParentEditor();
				element = editor.document.createElement( 'textarea' );
			}
			this.commitContent( element );

			if ( isInsertMode )
				editor.insertElement( element );
		},
		contents : [
			{
				id : 'info',
				label : editor.lang.textarea.title,
				title : editor.lang.textarea.title,
				elements : [
					{
						id : '_cke_saved_name',
						type : 'text',
						label : editor.lang.common.name,
						'default' : '',
						accessKey : 'N',
						setup : function( element )
						{
							this.setValue(
									element.data( 'cke-saved-name' ) ||
									element.getAttribute( 'name' ) ||
									'' );
						},
						commit : function( element )
						{
							if ( this.getValue() )
								element.data( 'cke-saved-name', this.getValue() );
							else
							{
								element.data( 'cke-saved-name', false );
								element.removeAttribute( 'name' );
							}
						}
					},
					{
						type : 'hbox',
						widths:['50%','50%'],
						children:[
							{
								id : 'cols',
								type : 'text',
								label : editor.lang.textarea.cols,
								'default' : '',
								accessKey : 'C',
								style : 'width:50px',
								validate : CKEDITOR.dialog.validate.integer( editor.lang.common.validateNumberFailed ),
								setup : function( element )
								{
									var value = element.hasAttribute( 'cols' ) && element.getAttribute( 'cols' );
									this.setValue( value || '' );
								},
								commit : function( element )
								{
									if ( this.getValue() )
										element.setAttribute( 'cols', this.getValue() );
									else
										element.removeAttribute( 'cols' );
								}
							},
							{
								id : 'rows',
								type : 'text',
								label : editor.lang.textarea.rows,
								'default' : '',
								accessKey : 'R',
								style : 'width:50px',
								validate : CKEDITOR.dialog.validate.integer( editor.lang.common.validateNumberFailed ),
								setup : function( element )
								{
									var value = element.hasAttribute( 'rows' ) && element.getAttribute( 'rows' );
									this.setValue( value || '' );
								},
								commit : function( element )
								{
									if ( this.getValue() )
										element.setAttribute( 'rows', this.getValue() );
									else
										element.removeAttribute( 'rows' );
								}
							}
						]
					},
					{
						id : 'value',
						type : 'textarea',
						label : editor.lang.textfield.value,
						'default' : '',
						setup : function( element )
						{
							this.setValue( element.$.defaultValue );
						},
						commit : function( element )
						{
							element.$.value = element.$.defaultValue = this.getValue() ;
						}
					},
                                        
            
            
            
            
           
            {
                id : 'id',
                type : 'text',
                label :'ID(unique id of the element)',
                accessKey : 'I',
                'default' : '',
               setup : function( element )
								{
									var value = element.hasAttribute( 'id' ) && element.getAttribute( 'id' );
									this.setValue( value || '' );
								},
								commit : function( element )
								{
									if ( this.getValue() )
										element.setAttribute( 'id', this.getValue() );
									else
										element.removeAttribute( 'id' );
								}
            },   
                      
                      
                      
                                        {
                id : 'textarea_select',
                type : 'select',
                label : 'select_action',
                'default' : 'onmouseup',
						
                items :
                [
                [ 'onclick', 'onclick' ],
                [ 'onmousedown', 'onmousedown' ],
                [ 'onmouseup','onmouseup' ],                
                [ 'onchange', 'onchange' ],
                [ 'onblur','onblur' ],
                [ 'onfocus','onfocus' ],
                [ 'onmouseover','onmouseover' ],
                [ 'onmousemove','onmousemove' ],
                [ 'onmouseout','onmouseout' ],
                [ 'ondbclick','ondbclick' ],
                [ 'onkeydown','onkeydown' ],
                [ 'onkeyup','onkeyup' ],
                [ 'onkeypress','onkeypress' ]
                ]
						
						
            },
            {
                id : 'set_action',
                type : 'button',
                label : 'set action',
                onClick:function(){
                                                    
                    var id_val=CKEDITOR.dialog.getCurrent().getContentElement('info', 'id').getValue();
                    var action_val=CKEDITOR.dialog.getCurrent().getContentElement('info', 'textarea_select').getValue();
                    //alert(id_val+action_val);
                                                
                                                
                    get_event_handler(id_val,action_val);
                                               
                    CKEDITOR.dialog.getCurrent().hide();
                                                 
                                                 
                }
            }

				]
			}
		]
	};
});
