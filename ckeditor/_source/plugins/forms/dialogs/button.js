﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.dialog.add( 'button', function( editor )
{
    function commitAttributes( element )
    {
        var val = this.getValue();
        if ( val )
        {
            element.attributes[ this.id ] = val;
            if ( this.id == 'name' )
                element.attributes[ 'data-cke-saved-name' ] = val;
        }
        else
        {
            delete element.attributes[ this.id ];
            if ( this.id == 'name' )
                delete element.attributes[ 'data-cke-saved-name' ];
        }
    }

    return {
        title : editor.lang.button.title,
        minWidth : 350,
        minHeight : 150,
        onShow : function()
        {
            delete this.button;
            var element = this.getParentEditor().getSelection().getSelectedElement();
            if ( element && element.is( 'input' ) )
            {
                var type = element.getAttribute( 'type' );
                if ( type in {
                    button:1, 
                    reset:1, 
                    submit:1
                } )
{
                    this.button = element;
                    this.setupContent( element );
                }
            }
        },
        onOk : function()
        {
            var editor = this.getParentEditor(),
            element = this.button,
            isInsertMode = !element;

            var fake = element ? CKEDITOR.htmlParser.fragment.fromHtml( element.getOuterHtml() ).children[ 0 ]
            : new CKEDITOR.htmlParser.element( 'input' );
            this.commitContent( fake );

            var writer = new CKEDITOR.htmlParser.basicWriter();
            fake.writeHtml( writer );
            var newElement = CKEDITOR.dom.element.createFromHtml( writer.getHtml(), editor.document );

            if ( isInsertMode )
                editor.insertElement( newElement );
            else
            {
                newElement.replace( element );
                editor.getSelection().selectElement( newElement );
            }
        },
        contents : [
        {
            id : 'info',
            label : editor.lang.button.title,
            title : editor.lang.button.title,
            elements : [
            {
                id : 'name',
                type : 'text',
                label : editor.lang.common.name,
                'default' : '',
                setup : function( element )
                {
                    this.setValue(
                        element.data( 'cke-saved-name' ) ||
                        element.getAttribute( 'name' ) ||
                        '' );
                },
                commit : commitAttributes
            },
            {
                id : 'value',
                type : 'text',
                label : editor.lang.button.text,
                accessKey : 'V',
                'default' : '',
                setup : function( element )
                {
                    this.setValue( element.getAttribute( 'value' ) || '' );
                },
                commit : commitAttributes
            },
                                        
            {
                id : 'id',
                type : 'text',
                label :'ID(unique id of the element)',
                accessKey : 'I',
                'default' : '',
                setup : function( element )
                {
                    this.setValue( element.getAttribute( 'id' ) || '' );
                },
                commit : commitAttributes
            },
                                        
                                        
            {
                id : 'type',
                type : 'select',
                label : editor.lang.button.type,
                'default' : 'button',
                accessKey : 'T',
                items :
                [
                [ editor.lang.button.typeBtn, 'button' ],
                [ editor.lang.button.typeSbm, 'submit' ],
                [ editor.lang.button.typeRst, 'reset' ]
                ],
                setup : function( element )
                {
                    this.setValue( element.getAttribute( 'type' ) || '' );
                },
                commit : commitAttributes
            },
                                        
            {
                id : 'action',
                type : 'select',
                label : 'select action',
                'default' : 'onmouseup',
						
                items :
                [
                [ 'onclick', 'onclick' ],
                [ 'onmousedown', 'onmousedown' ],
                [ 'onmouseup','onmouseup' ],                
                [ 'onchange', 'onchange' ],
                [ 'onblur','onblur' ],
                [ 'onfocus','onfocus' ],
                [ 'onmouseover','onmouseover' ],
                [ 'ondbclick','ondbclick' ]
                ]
						
            },
            {
                id : 'set_action',
                type : 'button',
                label : 'set action',
                onClick:function(){
                                                    
                    var id_val=CKEDITOR.dialog.getCurrent().getContentElement('info', 'id').getValue();
                    var action_val=CKEDITOR.dialog.getCurrent().getContentElement('info', 'action').getValue();
                    //alert(id_val+action_val);
                                                
                                                
                    get_event_handler(id_val,action_val);
                                               
                    CKEDITOR.dialog.getCurrent().hide();
                                                 
                                                 
                }
						
			
            }
                                        
                                        
                                        
            ]
        }
        ]
    };
});
